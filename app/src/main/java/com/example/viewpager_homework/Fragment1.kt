package com.example.viewpager_homework

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment


class FragmentOne : Fragment (R.layout.fragment1) {

    private lateinit var noteEditText: EditText
    private lateinit var buttonAdd: Button
    private lateinit var textView: TextView
    private lateinit var sharedPreferences: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)
        registerListener()

        sharedPreferences = view.context.getSharedPreferences("MY_APP_P", MODE_PRIVATE)
        val text = sharedPreferences.getString("NOTES", "")
        textView.text = text

    }

    private fun init(view: View) {
        textView = view.findViewById(R.id.textView)
        buttonAdd = view.findViewById(R.id.buttonAdd)
        noteEditText = view.findViewById(R.id.noteEditText22)
    }

    private fun registerListener() {
        buttonAdd.setOnClickListener {

            val note = textView.text.toString()
            val text = noteEditText.text.toString()
            val result = note+"\n"+text

            if (text.isNotEmpty()) {

                textView.text = result
                sharedPreferences.edit()
                    .putString("NOTES", result)
                    .apply()
                noteEditText.text.clear()
            }else {
                Toast.makeText(activity, "შეცდომააა!", Toast.LENGTH_SHORT).show()
            }

        }

    }
}